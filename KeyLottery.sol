pragma solidity ^0.4.23;

contract KeyLottery{
    
    struct Key{
        string driver;
        uint carNumber;
    }
    
    Key[] Keys;
    mapping(address => uint ) GirlToCar;
    
    function setKey(string _driver,uint _carNumber) {
      //if Keys[] counts >10 then showmessage('Too many keys') else
      Keys.push(Key(_driver,_carNumber));    
    }
    
    function setGirl() {
       address lotteryGirl = msg.sender;
       //randon get Keys id ,when i learn about randon, replace it 
       uint i = 2;
       //check if the Girl(addr) is mapped to an id 
       GirlToCar[lotteryGirl] = i;
        
        
    }
    function getKey() view returns(string) {
        
       uint id = GirlToCar[msg.sender];
       return Keys[id-1].driver;
    }
    
}